resource "aws_ecr_repository" "quotes" {
  name                 = "quotes"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}
