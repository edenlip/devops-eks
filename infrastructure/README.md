This directory should be used to manage an EKS infrastructure.

There are two different workspaces as per the best practice for EKS, one for base infra and one for apps inside EKS.

## AWS Infrastructure
The Terraform configuration in the sub directory named 'base' sets up the following resources on AWS:

* A new VPC
* An EKS cluster named devops-test
* An ECR repository

### EKS workspace will create:
* cloudwatch deployment in namespace amazon-cloudwatch 
* Quote app that intergte 2 microservices is deployed in quotes namespace.

you can get the lb endpoint via the <b ><quote_generator_load_balancer_hostname> OR <quote_composer_load_balancer_hostname> </b> output.


## Prerequisites
Before running the Terraform configuration, make sure you have the following:

* AWS CLI installed and configured with appropriate credentials.
* Terraform CLI installed on your local machine.

## Deployment steps
1. Initialise the workspace
  ```
    terraform init
  ```
2. Apply the Terraform configuration:
  ```
    terraform apply
  ```

## CleanUp
please run 
  ```
    terraform destroy
  ```
  Note: Be cautious when using the destroy command as it will permanently delete the created resources.


## Directory structure
<root dir>/ </br>
├── base/</br>
│   │   ├── versions.tf</br>
│   │   ├── outputs.tf</br>
│   │   ├── data.tf</br>
│   │   ├── variables.tf</br>
│   │   ├── networking.tf</br>
│   │   ├── eks.tf</br>
│   │   └── ....</br>
│   ├── eks/</br>
│   │   ├── versions.tf</br>
│   │   ├── outputs.tf</br>
│   │   ├── data.tf</br>
│   │   ├── variables.tf</br>
│   │   ├── quotes.tf</br>
│   │   └── ....</br>

## Next steps And How To Improve:

### Improved Design:
Evaluate and enhance the current design to ensure scalability, resilience, and security.

### Scalability:
* Switch to KEDA: Consider switching from Horizontal Pod Autoscaler (HPA) to Kubernetes 
Event-driven Autoscaling (KEDA) for more efficient autoscaling based on external metrics.

#### Fault Tolerance:

* Consider adding metrics and log collection using Prometheus to monitor performance and usage.

These improvements aim to optimize the infrastructure's performance, reliability, and maintainability for long-term operation.
