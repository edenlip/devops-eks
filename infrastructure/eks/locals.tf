locals {
  aws_region = "us-east-1"

  extra_tags = {
    TerraformManaged = "True"
    Environment      = local.env_name
  }

  env_name = "devops-test"

  quote_generator_port = 5000

  quote_composer_port = 6000

  quote_composer_image_tag = "latest"

  quote_generator_image_tag = "latest"

  quote_generator_lb_name = split("-", split(".", kubernetes_service.quote_generator.status.0.load_balancer.0.ingress.0.hostname).0).0

  quote_composer_lb_name = split("-", split(".", kubernetes_service.quote_composer.status.0.load_balancer.0.ingress.0.hostname).0).0
}
