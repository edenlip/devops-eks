resource "kubernetes_namespace" "quotes_namespace" {
  metadata {
    name = "quotes-namespace"
  }
}

resource "kubernetes_deployment" "quote_generator_deployment" {
  metadata {
    name      = "quote-generator"
    namespace = kubernetes_namespace.quote_namespace.metadata[0].name
  }

  spec {
    replicas = 3

    selector {
      match_labels = {
        app = "quote-generator"
      }
    }

    template {
      metadata {
        labels = {
          app = "quote-generator"
        }
      }

      spec {
        container {
          image = "${data.aws_ecr_repository.quotes.repository_url}/quote-generator:${data.aws_ecr_image.quote_generator_image.image_tag}"
          name  = "quote-generator"
         
          port {
            container_port =  local.quote_generator_port
          }

          resources {
            limits = {
              cpu    = "500m"
              memory = "128Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "64Mi"
            }
          }

          liveness_probe {
            http_get {
              path = "/health"
              port = local.quote_generator_port
            }

            initial_delay_seconds = 15
            period_seconds        = 5
            timeout_seconds       = 2
            failure_threshold     = 3
          }

          readiness_probe {
            http_get {
              path = "/health"
              port = local.quote_generator_port
            }

            initial_delay_seconds = 5
            period_seconds        = 5
            timeout_seconds       = 1
            failure_threshold     = 3
            success_threshold     = 1
          }

        }
      }
    }
  }
}

resource "kubernetes_deployment" "quote_composer_deployment" {
  metadata {
    name      = "quote-composer"
    namespace = kubernetes_namespace.quote_namespace.metadata[0].name
  }

  spec {
    replicas = 3

    selector {
      match_labels = {
        app = "quote-composer"
      }
    }

    template {
      metadata {
        labels = {
          app = "quote-composer"
        }
      }

      spec {
        container {
          image = "${data.aws_ecr_repository.quotes.repository_url}/quote-composer:${data.aws_ecr_image.quote_composer_image.image_tag}"
          name  = "quote-composer"
         
          port {
            container_port = local.quote_composer_port
          }

          resources {
            limits = {
              cpu    = "500m"
              memory = "128Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "64Mi"
            }
          }

          liveness_probe {
            http_get {
              path = "/health"
              port =  local.quote_composer_port
            }

            initial_delay_seconds = 15
            period_seconds        = 5
            timeout_seconds       = 2
            failure_threshold     = 3
          }

          readiness_probe {
            http_get {
              path = "/health"
              port =  local.quote_composer_port
            }

            initial_delay_seconds = 5
            period_seconds        = 5
            timeout_seconds       = 1
            failure_threshold     = 3
            success_threshold     = 1
          }

        }
      }
    }
  }
}

resource "kubernetes_service" "quote_generator_service" {
  metadata {
    name = "quote-generator"
    namespace = kubernetes_namespace.quotes_namespace.metadata[0].name
  }
  spec {
    selector = {
      app = "quote-generator"
    }

    port {
      port        = 5000
      target_port = 5000
    }

    type = "LoadBalancer"
  }
}

resource "kubernetes_service" "quote_composer_service" {
  metadata {
    name = "quote-composer"
    namespace = kubernetes_namespace.quotes_namespace.metadata[0].name
  }
  spec {
    selector = {
      app = "quote-composer"
    }

    port {
      port        = 6000
      target_port = 6000
    }

    type = "LoadBalancer"
  }
}

resource "kubectl_manifest" "quotes_manifests" {
  for_each  = data.kubectl_path_documents.quotes_manifests.manifests
  yaml_body = each.value
}
