data "aws_caller_identity" "current" {}

data "aws_eks_cluster_auth" "cluster" {
  name = "devops-test"
}

data "aws_eks_cluster" "cluster" {
  name = "devops-test"
}

data "kubectl_path_documents" "quotes_manifests" {
  pattern = "./manifests/quotes/*.yaml"
}

data "kubectl_path_documents" "amazon_cloudwatch_manifests" {
  pattern = "./manifests/cloudwatch/*.yaml"
}

data "aws_ecr_repository" "quotes" {
  name = "quotes"
}

data "aws_ecr_image" "quote_generator_image" {
  repository_name = "quotes/quote_generator"
  image_tag       = local.quote_generator_image_tag
}

data "aws_ecr_image" "quote_composer_image" {
  repository_name = "quotes/quote_composer"
  image_tag       = local.quote_composer_image_tag
}

data "aws_elb" "quote_generator" {
  name = local.quote_generator_lb_name
}

data "aws_elb" "quote_composer" {
  name = local.quote_composer_lb_name
}
