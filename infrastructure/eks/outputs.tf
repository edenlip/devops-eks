output "quote_generator_load_balancer_name" {
  value = local.quote_generator_lb_name
}

output "quote_generator_load_balancer_hostname" {
  value = kubernetes_service.quote_generator_service.status.0.load_balancer.0.ingress.0.hostname
}

output "quote_composer_load_balancer_name" {
  value = local.quote_composer_lb_name
}

output "quote_composer_load_balancer_hostname" {
  value = kubernetes_service.quote_composer_service.status.0.load_balancer.0.ingress.0.hostname
}
