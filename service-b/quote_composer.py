from flask import Flask, jsonify
import requests
import os
import logging

# Create Flask app instance
app = Flask(__name__)

# Setup logger
logging.basicConfig(level=logging.INFO)

# Environment variable for quote generator URL (service a)
QUOTE_GENERATOR_URL = os.getenv('QUOTE_GENERATOR_URL', "http://service-a:5000/quote")

# Function to get a composed quote
@app.route('/composed_quote', methods=['GET'])
def get_composed_quote():
    try:
        response = requests.get(QUOTE_GENERATOR_URL, timeout=5)  # Add timeout for requests
        if response.status_code == 200:
            quote_data = response.json()
            # Future improvement: integrate a dynamic authorship or formatting based on additional data
            composed_quote = f"{quote_data['quote']} - Dynamic Author"
            return jsonify({"composed_quote": composed_quote})
        else:
            logging.error(f"Error accessing Quote generator service : Status code {response.status_code}")
            return jsonify({"error": "Quote generator service A is unavailable"}), 503
    except Exception as e:
        logging.error(f"Error composing quote: {str(e)}")
        return jsonify({"error": "Internal server error"}), 500

# Health check endpoint for Kubernetes
@app.route('/health', methods=['GET'])
def health_check():
    return jsonify({"status": "healthy"}), 200

# Main function to run the Flask app
if __name__ == '__main__':
    port = os.getenv('QUOTE_COMPOSER_PORT', '6000')
    app.run(port=int(port), host='0.0.0.0', debug=False)  # Disable debug mode in production
