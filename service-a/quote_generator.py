from flask import Flask, jsonify
import random
import os
import logging

# Create Flask app instance
app = Flask(__name__)

# Setup logger
logging.basicConfig(level=logging.INFO)

# Load quotes from an external file or service for better modularity
try:
    with open('quotes.txt', 'r') as file:
        quotes = file.read().splitlines()
except FileNotFoundError:
    logging.error("Quotes file not found.")
    quotes = []  # Fallback to an empty list if file is missing

# Select a random quote from the predefined list
def select_random_quote():
    if quotes:
        return random.choice(quotes)
    else:
        return "No quotes available."

# Define endpoint to return a random quote
@app.route('/quote', methods=['GET'])
def get_quote():
    try:
        quote = select_random_quote()
        return jsonify({"quote": quote})
    except Exception as e:
        logging.error(f"Error getting quote: {str(e)}")
        return jsonify({"error": "Internal server error"}), 500

# Health check endpoint for Kubernetes probes
@app.route('/health', methods=['GET'])
def health_check():
    return jsonify({"status": "healthy"}), 200

# Run the Flask app
if __name__ == '__main__':
    port = os.getenv('QUOTE_GENERATOR_PORT', '5000')
    app.run(port=int(port), host='0.0.0.0', debug=False)  # Disable debug in production
